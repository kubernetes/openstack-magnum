From d0c1021293dd5356b04df1c6cab19aa9b487a084 Mon Sep 17 00:00:00 2001
From: Diogo Guerra <dy090.guerra@gmail.com>
Date: Wed, 18 Dec 2019 14:21:11 +0100
Subject: [PATCH] [cern] Add EOS provisioner configuration

This includes two new labels:
* eos_enabled: default to False, either the provisioner should be
configured
* eos_chart_tag: the helm chart tag to install

Change-Id: Ibbd5c7c77f17139b5ced6f419488f32bee9d31a5
---
 .../fragments/write-heat-params-master.sh     |   2 +
 .../kubernetes/helm/cern-eos-provisioner.sh   | 115 ++++++++++++++++++
 .../drivers/heat/k8s_fedora_template_def.py   |   3 +-
 .../templates/kubecluster.yaml                |  13 ++
 .../templates/kubemaster.yaml                 |  10 ++
 .../templates/kubecluster.yaml                |  13 ++
 .../templates/kubemaster.yaml                 |  10 ++
 7 files changed, 165 insertions(+), 1 deletion(-)
 create mode 100644 magnum/drivers/common/templates/kubernetes/helm/cern-eos-provisioner.sh

diff --git a/magnum/drivers/common/templates/kubernetes/fragments/write-heat-params-master.sh b/magnum/drivers/common/templates/kubernetes/fragments/write-heat-params-master.sh
index f766435ca83088801270d839da5d257f53ac4464..659c444c9c44984d6395817dcb72b9406f5756df 100644
--- a/magnum/drivers/common/templates/kubernetes/fragments/write-heat-params-master.sh
+++ b/magnum/drivers/common/templates/kubernetes/fragments/write-heat-params-master.sh
@@ -111,6 +111,8 @@ METRICS_PRODUCER="$METRICS_PRODUCER"
 METRICS_PRODUCER_VERSION="$METRICS_PRODUCER_VERSION"
 KUBE_CSI_ENABLED="$KUBE_CSI_ENABLED"
 KUBE_CSI_VERSION="$KUBE_CSI_VERSION"
+EOS_ENABLED="$EOS_ENABLED"
+EOS_CHART_TAG="$EOS_CHART_TAG"
 CEPHFS_CSI_ENABLED="$CEPHFS_CSI_ENABLED"
 CEPHFS_CSI_VERSION="$CEPHFS_CSI_VERSION"
 CVMFS_CSI_ENABLED="$CVMFS_CSI_ENABLED"
diff --git a/magnum/drivers/common/templates/kubernetes/helm/cern-eos-provisioner.sh b/magnum/drivers/common/templates/kubernetes/helm/cern-eos-provisioner.sh
new file mode 100644
index 0000000000000000000000000000000000000000..5ccd66e4c33821d9eac1c2e84684219443bfca9a
--- /dev/null
+++ b/magnum/drivers/common/templates/kubernetes/helm/cern-eos-provisioner.sh
@@ -0,0 +1,115 @@
+#!/bin/bash
+
+. /etc/sysconfig/heat-params
+
+set -ex
+
+step="cern-eos-provisioner"
+printf "Starting to run ${step}\n"
+
+### Configuration
+###############################################################################
+CHART_NAME="eosxd"
+
+# Check if prometheus monitoring is enabled and if user specified a METRICS_PRODUCER
+if [ "$(echo ${EOS_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ]; then
+    HELM_MODULE_CONFIG_FILE="/srv/magnum/kubernetes/helm/${CHART_NAME}.yaml"
+    [ -f ${HELM_MODULE_CONFIG_FILE} ] || {
+        echo "Writing File: ${HELM_MODULE_CONFIG_FILE}"
+        mkdir -p $(dirname ${HELM_MODULE_CONFIG_FILE})
+        cat << EOF > ${HELM_MODULE_CONFIG_FILE}
+---
+kind: ConfigMap
+apiVersion: v1
+metadata:
+  name: ${CHART_NAME}-config
+  namespace: magnum-tiller
+  labels:
+    app: helm
+data:
+  install-${CHART_NAME}.sh: |
+    #!/bin/bash
+    set -ex
+    mkdir -p \${HELM_HOME}
+    cp /etc/helm/* \${HELM_HOME}
+
+    # HACK - Force wait because of bug https://github.com/helm/helm/issues/5170
+    until helm init --client-only --wait
+    do
+        sleep 5s
+    done
+    helm repo add cern https://registry.cern.ch/chartrepo/cern
+    helm repo update
+
+    if [[ \$(helm history ${CHART_NAME} | grep ${CHART_NAME}) ]]; then
+        echo "${CHART_NAME} already installed on server. Continue..."
+        exit 0
+    else
+        helm install cern/${CHART_NAME} --namespace kube-system --name ${CHART_NAME} --version ${EOS_CHART_TAG} --values /opt/magnum/install-${CHART_NAME}-values.yaml
+    fi
+
+  install-${CHART_NAME}-values.yaml:  |
+    mounts:
+      ams:
+      atlas:
+      cms:
+      experiment:
+      lhcb:
+      project:
+        # project-i00: "a e j g v k q y"
+        # project-i01: "l h b p s f w n o"
+        # project-i02: "d c i r m t u x z"
+      theory:
+      user:
+        # home-i00: "l n t z"
+        # home-i01: "a g j k w"
+        # home-i02: "h o r s y"
+        # home-i03: "b e m v x"
+        # home-i04: "c f i p q"
+      workspace:
+
+---
+
+apiVersion: batch/v1
+kind: Job
+metadata:
+  name: install-${CHART_NAME}-job
+  namespace: magnum-tiller
+spec:
+  backoffLimit: 5
+  template:
+    spec:
+      serviceAccountName: tiller
+      containers:
+      - name: config-helm
+        image: ${CONTAINER_INFRA_PREFIX:-docker.io/openstackmagnum/}helm-client:dev
+        command:
+        - bash
+        args:
+        - /opt/magnum/install-${CHART_NAME}.sh
+        env:
+        - name: HELM_HOME
+          value: /helm_home
+        - name: TILLER_NAMESPACE
+          value: magnum-tiller
+        - name: HELM_TLS_ENABLE
+          value: "true"
+        volumeMounts:
+        - name: install-${CHART_NAME}-config
+          mountPath: /opt/magnum/
+        - mountPath: /etc/helm
+          name: helm-client-certs
+      restartPolicy: Never
+      volumes:
+      - name: install-${CHART_NAME}-config
+        configMap:
+          name: ${CHART_NAME}-config
+      - name: helm-client-certs
+        secret:
+          secretName: helm-client-secret
+EOF
+    }
+
+fi
+
+printf "Finished running ${step}\n"
diff --git a/magnum/drivers/heat/k8s_fedora_template_def.py b/magnum/drivers/heat/k8s_fedora_template_def.py
index cf73147facbb2339ba9740bc1eae4714236f5e90..f79e77c3fa96f56738205e1aad48f6f850fba4bd 100644
--- a/magnum/drivers/heat/k8s_fedora_template_def.py
+++ b/magnum/drivers/heat/k8s_fedora_template_def.py
@@ -112,7 +112,8 @@ class K8sFedoraTemplateDefinition(k8s_template_def.K8sTemplateDefinition):
                       'logging_chart_tag', 'kube_csi_enabled',
                       'kube_csi_version', 'cephfs_csi_enabled',
                       'cephfs_csi_version', 'cvmfs_csi_enabled',
-                      'cvmfs_csi_version', 'manila_enabled', 'manila_version']
+                      'cvmfs_csi_version', 'manila_enabled', 'manila_version',
+                      'eos_enabled', 'eos_chart_tag',]
 
         labels = self._get_relevant_labels(cluster, kwargs)
 
diff --git a/magnum/drivers/k8s_fedora_atomic_v1/templates/kubecluster.yaml b/magnum/drivers/k8s_fedora_atomic_v1/templates/kubecluster.yaml
index c544d733232353bea6c2d593e64c8c86fcf0ba2f..341c37311ac0130aa3ad03b5562bf65901b2d787 100644
--- a/magnum/drivers/k8s_fedora_atomic_v1/templates/kubecluster.yaml
+++ b/magnum/drivers/k8s_fedora_atomic_v1/templates/kubecluster.yaml
@@ -781,6 +781,16 @@ parameters:
       Indicates the version of CSI to support.
     default: "v0.2.0"
 
+  eos_enabled:
+    type: boolean
+    description: Indicates whether the eos csi plugin should be started.
+    default: false
+
+  eos_chart_tag:
+    type: string
+    description: The cern/eosxd chart version to use.
+    default: "v0.2.0"
+
   cephfs_csi_enabled:
     type: boolean
     description: >
@@ -1057,6 +1067,8 @@ resources:
           metrics_producer_version: {get_param: metrics_producer_version}
           kube_csi_enabled: {get_param: kube_csi_enabled}
           kube_csi_version: {get_param: kube_csi_version}
+          eos_enabled: {get_param: eos_enabled}
+          eos_chart_tag: {get_param: eos_chart_tag}
           cephfs_csi_enabled: {get_param: cephfs_csi_enabled}
           cephfs_csi_version: {get_param: cephfs_csi_version}
           cvmfs_csi_enabled: {get_param: cvmfs_csi_enabled}
@@ -1138,6 +1150,7 @@ resources:
             - get_file: ../../common/templates/kubernetes/helm/ingress-nginx.sh
             - get_file: ../../common/templates/kubernetes/helm/cern-prometheus-rules.sh
             - get_file: ../../common/templates/kubernetes/helm/cern-central-monitoring-logging.sh
+            - get_file: ../../common/templates/kubernetes/helm/cern-eos-provisioner.sh
             - get_file: ../../common/templates/kubernetes/fragments/install-helm-modules.sh
 
   kube_cluster_deploy:
diff --git a/magnum/drivers/k8s_fedora_atomic_v1/templates/kubemaster.yaml b/magnum/drivers/k8s_fedora_atomic_v1/templates/kubemaster.yaml
index 871e8ef16acb83b4fe45d1fc2c613b298200edee..bc6fff6e79cb5f3907ecf195b61ba284c19cdabd 100644
--- a/magnum/drivers/k8s_fedora_atomic_v1/templates/kubemaster.yaml
+++ b/magnum/drivers/k8s_fedora_atomic_v1/templates/kubemaster.yaml
@@ -444,6 +444,14 @@ parameters:
       Indicates the version of CSI to support.
     default: "v0.2.0"
 
+  eos_enabled:
+    type: boolean
+    description: Indicates whether the eos csi plugin should be started.
+
+  eos_chart_tag:
+    type: string
+    description: The cern/eosxd chart version to use.
+
   cephfs_csi_enabled:
     type: boolean
     description: >
@@ -826,6 +834,8 @@ resources:
                   "$METRICS_PRODUCER_VERSION": {get_param: metrics_producer_version}
                   "$KUBE_CSI_ENABLED": {get_param: kube_csi_enabled}
                   "$KUBE_CSI_VERSION": {get_param: kube_csi_version}
+                  "$EOS_ENABLED": {get_param: eos_enabled}
+                  "$EOS_CHART_TAG": {get_param: eos_chart_tag}
                   "$CEPHFS_CSI_ENABLED": {get_param: cephfs_csi_enabled}
                   "$CEPHFS_CSI_VERSION": {get_param: cephfs_csi_version}
                   "$CVMFS_CSI_ENABLED": {get_param: cvmfs_csi_enabled}
diff --git a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml
index 2fe09f741eeb7c2c910018d0a4ec76ef047a6dd5..f66c3220509e56ec390ebef699ddf2f5e0e6c752 100644
--- a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml
+++ b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml
@@ -781,6 +781,16 @@ parameters:
       Indicates the version of CSI to support.
     default: "v0.2.0"
 
+  eos_enabled:
+    type: boolean
+    description: Indicates whether the eos csi plugin should be started.
+    default: false
+
+  eos_chart_tag:
+    type: string
+    description: The cern/eosxd chart version to use.
+    default: "v0.2.0"
+
   cephfs_csi_enabled:
     type: boolean
     description: >
@@ -1058,6 +1068,8 @@ resources:
           metrics_producer_version: {get_param: metrics_producer_version}
           kube_csi_enabled: {get_param: kube_csi_enabled}
           kube_csi_version: {get_param: kube_csi_version}
+          eos_enabled: {get_param: eos_enabled}
+          eos_chart_tag: {get_param: eos_chart_tag}
           cephfs_csi_enabled: {get_param: cephfs_csi_enabled}
           cephfs_csi_version: {get_param: cephfs_csi_version}
           cvmfs_csi_enabled: {get_param: cvmfs_csi_enabled}
@@ -1141,6 +1153,7 @@ resources:
             - get_file: ../../common/templates/kubernetes/helm/ingress-nginx.sh
             - get_file: ../../common/templates/kubernetes/helm/cern-prometheus-rules.sh
             - get_file: ../../common/templates/kubernetes/helm/cern-central-monitoring-logging.sh
+            - get_file: ../../common/templates/kubernetes/helm/cern-eos-provisioner.sh
             - get_file: ../../common/templates/kubernetes/fragments/install-helm-modules.sh
 
   kube_cluster_deploy:
diff --git a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml
index c165e91eb3b34790324bb31fc7d385c3d3972801..e48d38d0f487a36eac41c6c876c37eab514d8b24 100644
--- a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml
+++ b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml
@@ -436,6 +436,14 @@ parameters:
       Indicates the version of CSI to support.
     default: "v0.2.0"
 
+  eos_enabled:
+    type: boolean
+    description: Indicates whether the eos csi plugin should be started.
+
+  eos_chart_tag:
+    type: string
+    description: The cern/eosxd chart version to use.
+
   cephfs_csi_enabled:
     type: boolean
     description: >
@@ -830,6 +838,8 @@ resources:
                   "$METRICS_PRODUCER_VERSION": {get_param: metrics_producer_version}
                   "$KUBE_CSI_ENABLED": {get_param: kube_csi_enabled}
                   "$KUBE_CSI_VERSION": {get_param: kube_csi_version}
+                  "$EOS_ENABLED": {get_param: eos_enabled}
+                  "$EOS_CHART_TAG": {get_param: eos_chart_tag}
                   "$CEPHFS_CSI_ENABLED": {get_param: cephfs_csi_enabled}
                   "$CEPHFS_CSI_VERSION": {get_param: cephfs_csi_version}
                   "$CVMFS_CSI_ENABLED": {get_param: cvmfs_csi_enabled}
