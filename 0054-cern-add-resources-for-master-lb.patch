From 5b2f5ac04c4a450079c6d854a97309b57855bace Mon Sep 17 00:00:00 2001
From: Ricardo Rocha <rocha.porto@gmail.com>
Date: Wed, 22 Apr 2020 21:43:14 +0200
Subject: [PATCH] [cern] add resources for master lb

Readd dropped resources to enable lb instances for api and etcd on the
master, as well as the required pool and members to register each master
with the lbs.
---
 .../kubernetes/fragments/make-cert.sh         |  5 -
 .../templates/kubecluster.yaml                | 94 +++++++++++++++++--
 .../templates/kubemaster.yaml                 | 24 ++++-
 .../templates/kubeminion.yaml                 |  4 +-
 4 files changed, 107 insertions(+), 20 deletions(-)

diff --git a/magnum/drivers/common/templates/kubernetes/fragments/make-cert.sh b/magnum/drivers/common/templates/kubernetes/fragments/make-cert.sh
index 2ffa7abf2b6b8c695cc67e390eef5e8a9831edcf..cfccfc93180d369321411101208f55313c348fa5 100644
--- a/magnum/drivers/common/templates/kubernetes/fragments/make-cert.sh
+++ b/magnum/drivers/common/templates/kubernetes/fragments/make-cert.sh
@@ -47,11 +47,6 @@ if [ -n "${KUBE_NODE_PUBLIC_IP}" ]; then
     sans="${sans},IP:${KUBE_NODE_PUBLIC_IP}"
 fi
 
-KUBE_NODE_PUBLIC_IP=${KUBE_NODE_IP}
-KUBE_API_PRIVATE_ADDRESS=${KUBE_NODE_IP}
-KUBE_API_PUBLIC_ADDRESS=${KUBE_NODE_PUBLIC_IP}
-
-sans="IP:${KUBE_NODE_PUBLIC_IP},IP:${KUBE_NODE_IP}"
 if [ "${KUBE_NODE_PUBLIC_IP}" != "${KUBE_API_PUBLIC_ADDRESS}" ] \
         && [ -n "${KUBE_API_PUBLIC_ADDRESS}" ]; then
     sans="${sans},IP:${KUBE_API_PUBLIC_ADDRESS}"
diff --git a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml
index 78b52c1481007ff54692031056654395742eb971..f59764221510e3c9ec4265bd9152910d24756dcb 100644
--- a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml
+++ b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubecluster.yaml
@@ -1000,6 +1000,69 @@ parameters:
 
 resources:
 
+  network:
+    condition: create_cluster_resources
+    type: ../../common/templates/network.yaml
+    properties:
+      existing_network: {get_param: fixed_network}
+      existing_subnet: {get_param: fixed_subnet}
+      private_network_cidr: {get_param: fixed_network_cidr}
+      dns_nameserver: {get_param: dns_nameserver}
+      external_network: {get_param: external_network}
+      private_network_name: {get_param: fixed_network_name}
+
+  api_lb:
+    condition: create_cluster_resources
+    type: ../../common/templates/lb_api.yaml
+    properties:
+      fixed_subnet: "public-subnet-1"
+      external_network: {get_param: external_network}
+      protocol: {get_param: loadbalancing_protocol}
+      port: {get_param: kubernetes_port}
+
+  etcd_lb:
+    condition: create_cluster_resources
+    type: ../../common/templates/lb_etcd.yaml
+    properties:
+      fixed_subnet: "public-subnet-1"
+      protocol: {get_param: loadbalancing_protocol}
+      port: 2379
+
+  ######################################################################
+  #
+  # resources that expose the IPs of either the kube master or a given
+  # LBaaS pool depending on whether LBaaS is enabled for the cluster.
+  #
+
+  api_address_lb_switch:
+    condition: create_cluster_resources
+    type: Magnum::ApiGatewaySwitcher
+    properties:
+      pool_public_ip: {get_attr: [api_lb, floating_address]}
+      pool_private_ip: {get_attr: [api_lb, address]}
+      master_public_ip: {get_attr: [kube_masters, resource.0.kube_master_external_ip]}
+      master_private_ip: {get_attr: [kube_masters, resource.0.kube_master_ip]}
+
+  etcd_address_lb_switch:
+    condition: create_cluster_resources
+    type: Magnum::ApiGatewaySwitcher
+    properties:
+      pool_private_ip: {get_attr: [etcd_lb, address]}
+      master_private_ip: {get_attr: [kube_masters, resource.0.kube_master_ip]}
+
+  ######################################################################
+  #
+  # resources that expose the IPs of either floating ip or a given
+  # fixed ip depending on whether FloatingIP is enabled for the cluster.
+  #
+
+  api_address_floating_switch:
+    condition: create_cluster_resources
+    type: Magnum::FloatingIPAddressSwitcher
+    properties:
+      public_ip: {get_attr: [api_address_lb_switch, public_ip]}
+      private_ip: {get_attr: [api_address_lb_switch, private_ip]}
+
   ######################################################################
   #
   # resources that expose the server group for all nodes include master
@@ -1027,6 +1090,8 @@ resources:
   kube_masters:
     condition: master_only
     type: OS::Heat::ResourceGroup
+    depends_on:
+      - network
     update_policy:
       rolling_update: {max_batch_size: {get_param: update_max_batch_size}, pause_time: 30}
     properties:
@@ -1045,8 +1110,8 @@ resources:
           metrics_server_chart_tag: {get_param: metrics_server_chart_tag}
           prometheus_monitoring: {get_param: prometheus_monitoring}
           grafana_admin_passwd: {get_param: grafana_admin_passwd}
-          api_public_address: ""
-          api_private_address: ""
+          api_public_address: {get_attr: [api_lb, floating_address]}
+          api_private_address: {get_attr: [api_lb, address]}
           ssh_key_name: {get_param: ssh_key_name}
           ssh_public_key: {get_param: ssh_public_key}
           server_image: {get_param: master_image}
@@ -1075,11 +1140,11 @@ resources:
           traefik_ingress_controller_tag: {get_param: traefik_ingress_controller_tag}
           volume_driver: {get_param: volume_driver}
           region_name: {get_param: region_name}
-          fixed_network: ""
+          fixed_network: {get_attr: [network, fixed_network]}
           fixed_network_name: {get_param: fixed_network_name}
-          fixed_subnet: ""
-          api_pool_id: ""
-          etcd_pool_id: ""
+          fixed_subnet: {get_attr: [network, fixed_subnet]}
+          api_pool_id: {get_attr: [api_lb, pool_id]}
+          etcd_pool_id: {get_attr: [etcd_lb, pool_id]}
           username: {get_param: username}
           password: {get_param: password}
           kubernetes_port: {get_param: kubernetes_port}
@@ -1106,6 +1171,7 @@ resources:
           auth_url: {get_param: auth_url}
           insecure_registry_url: {get_param: insecure_registry_url}
           container_infra_prefix: {get_param: container_infra_prefix}
+          etcd_lb_vip: {get_attr: [etcd_lb, address]}
           dns_service_ip: {get_param: dns_service_ip}
           dns_cluster_domain: {get_param: dns_cluster_domain}
           openstack_ca: ""
@@ -1272,6 +1338,8 @@ resources:
   kube_minions:
     condition: worker_only
     type: OS::Heat::ResourceGroup
+    depends_on:
+      - network
     update_policy:
       rolling_update: {max_batch_size: {get_param: update_max_batch_size}, pause_time: 30}
     properties:
@@ -1291,8 +1359,16 @@ resources:
           ssh_public_key: {get_param: ssh_public_key}
           server_image: {get_param: minion_image}
           minion_flavor: {get_param: minion_flavor}
-          fixed_network: ""
-          fixed_subnet: ""
+          fixed_network:
+            if:
+              - create_cluster_resources
+              - get_attr: [network, fixed_network]
+              - get_param: fixed_network
+          fixed_subnet:
+            if:
+              - create_cluster_resources
+              - get_attr: [network, fixed_subnet]
+              - get_param: fixed_subnet
           network_driver: {get_param: network_driver}
           flannel_network_cidr: {get_param: flannel_network_cidr}
           kube_master_ip:
@@ -1377,7 +1453,7 @@ outputs:
       str_replace:
         template: api_ip_address
         params:
-          api_ip_address: {get_attr: [kube_masters, resource.0.kube_master_external_ip]}
+          api_ip_address: {get_attr: [api_address_floating_switch, ip_address]}
     description: >
       This is the API endpoint of the Kubernetes cluster. Use this to access
       the Kubernetes API.
diff --git a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml
index 04bfe843b2b9b6d11d75ea4b46d8455ed24edcf8..70083e421e55f8eeeb5cbe59b8bd2d21f3b1e02a 100644
--- a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml
+++ b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubemaster.yaml
@@ -790,11 +790,11 @@ resources:
                   "$METRICS_SERVER_ENABLED": {get_param: metrics_server_enabled}
                   "$METRICS_SERVER_CHART_TAG": {get_param: metrics_server_chart_tag}
                   "$PROMETHEUS_MONITORING": {get_param: prometheus_monitoring}
-                  "$KUBE_API_PUBLIC_ADDRESS": ""
-                  "$KUBE_API_PRIVATE_ADDRESS":  ""
+                  "$KUBE_API_PUBLIC_ADDRESS": {get_param: api_public_address}
+                  "$KUBE_API_PRIVATE_ADDRESS":  {get_param: api_private_address}
                   "$KUBE_API_PORT": {get_param: kubernetes_port}
-                  "$KUBE_NODE_PUBLIC_IP": ""
-                  "$KUBE_NODE_IP": ""
+                  "$KUBE_NODE_PUBLIC_IP": {get_attr: [kube-master, first_address]}
+                  "$KUBE_NODE_IP": {get_attr: [kube-master, first_address]}
                   "$KUBE_ALLOW_PRIV": {get_param: kube_allow_priv}
                   "$ETCD_VOLUME": {get_resource: etcd_volume}
                   "$ETCD_VOLUME_SIZE": {get_param: etcd_volume_size}
@@ -998,6 +998,22 @@ resources:
           volume_id: {get_resource: kube_node_volume}
           delete_on_termination: true
 
+  api_pool_member:
+    type: Magnum::Optional::Neutron::LBaaS::PoolMember
+    properties:
+      pool: {get_param: api_pool_id}
+      address: {get_attr: [kube-master, first_address]}
+      #subnet: { get_param: fixed_subnet }
+      protocol_port: {get_param: kubernetes_port}
+
+  etcd_pool_member:
+    type: Magnum::Optional::Neutron::LBaaS::PoolMember
+    properties:
+      pool: {get_param: etcd_pool_id}
+      address: {get_attr: [kube-master, first_address]}
+      #subnet: { get_param: fixed_subnet }
+      protocol_port: 2379
+
   ######################################################################
   #
   # etcd storage.  This allocates a cinder volume and attaches it
diff --git a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubeminion.yaml b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubeminion.yaml
index a6930decdb2feee4ca1962dd95e4cd02b610e9ad..fd8ed2c9f62fd38dc74f9fa60d7f2e8d76ed5145 100644
--- a/magnum/drivers/k8s_fedora_coreos_v1/templates/kubeminion.yaml
+++ b/magnum/drivers/k8s_fedora_coreos_v1/templates/kubeminion.yaml
@@ -428,8 +428,8 @@ resources:
                   $KUBE_ALLOW_PRIV: {get_param: kube_allow_priv}
                   $KUBE_MASTER_IP: {get_param: kube_master_ip}
                   $KUBE_API_PORT: {get_param: kubernetes_port}
-                  $KUBE_NODE_PUBLIC_IP: ""
-                  $KUBE_NODE_IP: ""
+                  $KUBE_NODE_PUBLIC_IP: {get_attr: [kube-minion, first_address]}
+                  $KUBE_NODE_IP:  {get_attr: [kube-minion, first_address]}
                   $ETCD_SERVER_IP: {get_param: etcd_server_ip}
                   $DOCKER_VOLUME: {get_resource: docker_volume}
                   $DOCKER_VOLUME_SIZE: {get_param: docker_volume_size}
