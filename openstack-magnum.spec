# Macros for py2/py3 compatibility
%if 0%{?fedora} || 0%{?rhel} > 7
%global pyver %{python3_pkgversion}
%else
%global pyver 2
%endif
%global pyver_bin python%{pyver}
%global pyver_sitelib %python%{pyver}_sitelib
%global pyver_install %py%{pyver}_install
%global pyver_build %py%{pyver}_build
# End of macros for py2/py3 compatibility
%{!?upstream_version: %global upstream_version %{version}%{?milestone}}
%global with_doc %{!?_without_doc:1}%{?_without_doc:0}
%global service magnum

%global common_desc \
Magnum is an OpenStack project which provides a set of services for \
provisioning, scaling, and managing container orchestration engines.

Name:		openstack-%{service}
Summary:	Container Management project for OpenStack
Version:	9.2.0
Release:	1.29%{?dist}
License:	ASL 2.0
URL:		https://github.com/openstack/magnum.git

Source0:	https://tarballs.openstack.org/%{service}/%{service}-%{upstream_version}.tar.gz

Patch0: 0001-cern-setup-cern-ca-certificates.patch
Patch1: 0002-cern-drop-dependency-on-neutron-objects.patch
Patch2: 0003-cern-Make-werkzeug-log-the-client-IP-when-using-prox.patch
Patch3: 0004-cern-add-docker-cvmfs-driver-configuration.patch
Patch4: 0005-cern-add-cvmfs-storage-driver-config-to-swarm.patch
Patch5: 0006-cern-swarm-add-docker_ce_version-label.patch
Patch6: 0007-cern-Disable-host-group-affinity.patch
Patch7: 0008-cern-Add-central-monitoring-magnum-clusters-option.patch
Patch8: 0009-cern-Change-the-docker-socket-for-dockerd.patch
Patch9: 0010-cern-Get-minion-ip-from-metadata-or-host.patch
Patch10: 0011-cern-Add-csi-and-cephfs-cvmfs-kubernetes.patch
Patch11: 0012-cern-Add-Manila-Provisioner-configuration.patch
Patch12: 0013-cern-Disable-health-status.patch
Patch13: 0014-cern-Enable-K8s-Central-Monitoring.patch
Patch14: 0015-cern-Add-Central-Monitoring-Logging-from-Helm-chart.patch
Patch15: 0016-cern-Add-EOS-provisioner-configuration.patch
Patch16: 0017-cern-Add-Magnum-flag-for-landb-sync-install.patch
Patch17: 0018-cern-Add-calico_ipv4pool_ipip-label.patch
Patch18: 0019-cern-Fix-api-cert-manager-true-blocking-cluster-crea.patch
Patch19: 0020-cern-Drop-dependencies-on-neutron-objects.patch
Patch20: 0021-cern-Make-cern-setup-run-in-the-stein-heat-agent.patch
Patch21: 0022-cern-Add-selector-in-monitoring-deployments.patch
Patch22: 0023-cern-Execute-traefik-systemd-unit-over-ssh.patch
Patch23: 0024-cern-Make-feature-gates-configurable-with-KUBE_CSI.patch
Patch24: 0025-cern-Deploy-cern-hostcert-keytab-with-podman.patch
Patch25: 0026-cern-k8s_coreos-Set-REQUESTS_CA-for-heat-agent.patch
Patch26: 0027-cern-core-podman-Mount-os-release-properly.patch
Patch27: 0028-cern-k8s_coreos-pass-master-ip-to-minions.patch
Patch28: 0029-cern-Mount-root-kubelet-container.patch
Patch29: 0030-cern-Fix-ingress-traefik-systemd-unit.patch
Patch30: 0031-cern-Fix-cluster-resize-and-update.patch
Patch31: 0032-cern-Fix-traefik-deploy-systemd-unit.patch
Patch32: 0033-cern-Fix-cern_enabled-check.patch
Patch33: 0034-cern-disable-auto-update-in-coreos.patch
Patch34: 0035-cern-Update-cvmfs-and-fluentd-to-apps-v1.patch
Patch35: 0036-cern-Add-CMS-specific-config-to-cvmfs-setup.patch
Patch36: 0037-cern-Add-fluentd-selector-for-k8s-1.16.patch
Patch37: 0038-cern-calico-Add-node-status-in-ClusterRole.patch
Patch38: 0039-cern-fcos-podman-Set-max-size-for-logging-to-50m.patch
Patch39: 0040-cern-atomic-podman-Set-log-imit-to-50m.patch
Patch40: 0041-cern-Upgrade-pause-image-to-version-3.1.patch
Patch41: 0042-cern-k8s-fedora-Set-max-size-to-10m-for-containers.patch
Patch42: 0043-cern-Add-opt-in-containerd-support.patch
Patch43: 0044-cern-k8s-Improve-the-taint-of-master-node-kubelet.patch
Patch44: 0045-cern-k8s-Upgrade-default-coreDNS-version-to-1.6.6.patch
Patch45: 0046-k8s-Upgrade-calico-to-the-latest-stable-version.patch
Patch46: 0047-cern-Support-calico-v3.3.6.patch
Patch47: 0048-cern-fcos-kubelet-Add-rpc-statd-dependency.patch
Patch48: 0049-cern-Add-label-to-enable-nvidia-gpu.patch
Patch49: 0050-cern-Add-cern-chart-configuration.patch
Patch50: 0051-cern-Deploy-traefik-from-the-heat-agent.patch
Patch51: 0052-cern-Make-fluentd-work-with-selinux.patch
Patch52: 0053-cern-Readd-setting-for-server-group-affinity-policy.patch
Patch53: 0054-cern-add-resources-for-master-lb.patch
Patch54: 0055-cern-Labels-override.patch
Patch55: 0056-cern-Update-nginx-ingress-to-v1.36.3-and-0.32.0-tag.patch
Patch56: 0057-cern-Move-helm-components-from-src-to-umbrella-chart.patch
Patch57: 0058-cern-Set-csi-provisioner-replicas-to-1.patch
Patch58: 0059-cern-Use-public-subnet-2-for-lbaas-instances.patch
Patch59: 0060-cern-Add-cinder_csi_enabled-label.patch
Patch60: 0061-cern-Install-cinder-csi-from-cern-magnum-chart.patch
Patch61: 0062-cern-Add-base-to-cern_chart.patch
Patch62: 0063-cern-Set-containerRuntime-for-fluentd.patch
Patch63: 0064-cern-Fix-eos-for-pre-meta-chart-deployments.patch
Patch64: 0065-cern-wait-on-helm-install-of-umbrella-chart.patch
Patch65: 0066-cern-Add-CERN_CHART_ENABLED-in-write-heat-params.patch
Patch66: 0067-cern-Pass-gpu-tag-to-cern-helm-chart-values.patch
Patch67: 0068-cern-Set-coredns-cache-to-900-secs.patch
Patch68: 0069-cern-Configure-bin-packing-scheduler-policy-on-k8s-v.patch
Patch69: 0070-cern-k8s-Introduce-helm_client_tag-label.patch
Patch70: 0071-cern-set-charts-with-configurable-helm_client_tag.patch
Patch71: 0072-cern-Update-Cluster-Autoscaler-ClusterRole.patch
Patch72: 0073-cern-Configure-autoscaler-for-k8s-v1.19.patch
Patch73: 0074-cern-Set-network-id-cascade-in-lbaas-config.patch
Patch74: 0075-cern-Set-region-name-on-occm-config.patch
Patch75: 0076-cern-Increase-nofile-for-docker-to-1048576.patch
Patch76: 0077-cern-Replace-upstream-helm-charts-origin-repo.patch
Patch77: 0078-cern-Set-stable-chart-during-helm-init.patch
Patch78: 0079-cern-k8s-Add-admin.conf-kubeconfig.patch
Patch79: 0080-cern-k8s-fcos-Source-bashrc-for-clusterconfig.patch
Patch80: 0081-cern-Make-kubelet-and-kube-proxy-use-the-secure-port.patch
Patch81: 0082-k8s-Do-not-use-insecure-api-port.patch
Patch82: 0083-cern-Add-support-for-OIDC.patch
Patch83: 0084-cern-Fix-oidc_enabled.patch
Patch84: 0085-cern-k8s-Use-ClusterFirstWithHostNet-with-hostNetwor.patch
Patch85: 0086-cern-Pass-the-api_address-to-workers.patch
Patch86: 0087-cern-Add-keystone-wait-in-OCCM-init.patch
Patch87: 0088-cern-Fix-oidc_enabled.patch
Patch88: 0089-cern-Remove-shebang-from-scripts.patch
Patch89: 0090-cern-Poll-k-api-with-kubectl.patch
Patch90: 0091-cern-Fix-k-sch-config.patch
Patch91: 0092-cern-k8s-Make-metrics-server-work-without-DNS.patch
Patch92: 0093-cern-Add-ExtendedResourceToleration-to-default-admis.patch
Patch93: 0094-cern-manila-provisioner-deploy-only-if-MANILA_CSI_EN.patch
Patch94: 0095-cern-manila-provisioner-added-Meyrin-CephFS-Vault-SS.patch
Patch95: 0096-cern-Update-traefik-options.patch
Patch96: 0097-cern-Added-ceph-csi-cephfs.enabled-Helm-binding.patch
Patch97: 0098-cern-Added-labels-for-Manila-CSI.patch
Patch98: 0099-Install-cephfs-csi-from-CERN-metachart.patch
Patch99: 0100-removed-old-cephfs-csi.enabled-CEPHFS_CSI_ENABLED.patch
Patch100: 0101-cern-Drop-ubelet-https-from-kube-apiserver.patch
Patch101: 0102-cern-Set-cgroups-v1-explicitly-in-ignition.patch
Patch102: 0103-cern-Add-missing-manila_csi_-in-heat-params.patch
Patch103: 0104-cern-Add-ignition_version-label.patch
Patch104: 0105-cern-check-csi-cephfs-install-cern-chart-version.patch
Patch105: 0106-cern-Manually-mount-etc-hosts-in-kubelet.patch
Patch106: 0107-cern-Move-calico-CRD-version-to-v1.patch
Patch107: 0108-cern-k8s-Fix-docker-storage-of-Fedora-CoreOS.patch
Patch108: 0109-cern-Retry-when-requesting-discovery-url-fails.patch
Patch109: 0110-cern-move-cern-magnum-install-to-helm-v3.patch
Patch110: 0111-cern-Add-cern_chart_values-label.patch
Patch111: 0112-Set-log_size_max-for-podman.patch
Patch112: 0113-cern-Add-cert_manager_io_enabled-and-cert_manager_io.patch
Patch113: 0114-cern-Add-labels-for-snapshot-controller-and-validati.patch
Patch114: 0115-npd-via-helm.patch
Patch115: 0116-nginx-switches-for-cern-magnum.patch
Patch116: 0117-traefik-v2-from-cern-metachart.patch
Patch117: 0118-cern-Use-SNAPSHOT_VALIDATION_WEBHOOK_ENABLED-variabl.patch
Patch118: 0119-cern-Update-coredns-config-for-1.8.6.patch
Patch119: 0120-cern-Add-dual-stack-support.patch
Patch120: 0121-cern-fix-snapshot-params-in-kubecluster.patch
Patch121: 0122-cern-v1-for-authorization-resources-csidriver-cvmfs.patch
Patch122: 0123-cern-move-metrics-server-to-umbrella-chart.patch
Patch123: 0124-cern-enable-ipv6-on-1.22-clusters-only.patch
Patch124: 0125-cern-Configure-calico-ipppols-manually.patch
Patch125: 0126-cern-monitoring-from-upstream-chart.patch
Patch126: 0127-cern-configure-cvmfs-csi-via-umbrella-chart.patch
Patch127: 0128-cern-cloud-config-set-ignore-volume-az-true-for-Cind.patch
Patch128: 0129-cern-Set-official-for-OIDC-applications.patch
Patch129: 0130-cern-Add-master_availability_zones-label.patch
Patch130: 0131-cern-Check-tiller-enabled-when-installing-metachart.patch
Patch131: 0132-cern-cluster-autoscaler-via-helm.patch
Patch132: 0133-cern-set-clusterID-in-helm-values-for-cinder-csi-and.patch
Patch133: 0134-cern-Make-dual_stack-an-opt-out-feature.patch
Patch134: 0135-cern-Fix-master_availability_zones-for-k8s-FCOS-temp.patch
Patch135: 0136-cern-Add-kube-bench-disable-profilling.patch
Patch136: 0137-cern-Ensure-kube-apiserver-TLS-connection-to-etcd-se.patch
Patch137: 0138-cern-Fix-kube-bench-1.2.32-and-4.2.13.patch
Patch138: 0139-cern-Fix-kube-bench-1.2.1-1.2.23-1.3.1.patch
Patch139: 0140-install-kubernetes-dashboard-from-cern-metachart.patch
Patch140: 0141-Add-label-landb_sync_set.patch
Patch141: 0142-Add-etcd-secrets-to-kube-system-ns.patch
Patch142: 0143-cern-drop-removed-1.24-options-for-kubelet-and-sched.patch
Patch143: 0144-cern-coredns-via-helm.patch
Patch144: 0145-cern-openstack-cloud-controller-manager-via-helm.patch
Patch145: 0146-cern-Add-unit-toleration-and-hostnetwork-to-helm-job.patch
Patch146: 0147-cern-add-all-plugins-for-coredns-fix-occm-auth-url.patch
Patch147: 0148-cern-move-calico-to-helm.patch
Patch148: 0149-cern-allow-cern-magnum-install-from-repo-branch.patch
Patch149: 0150-cern-make-cipher-and-anonymous-false-only-1.24.patch
Patch150: 0151-cern-Add-values-for-gpu-operator-helm-chart.patch
Patch151: 0152-Populate-kube-controller-scheduler-proxy-IP.patch
Patch152: 0153-Fix-cern-enable-autoscaler-label-not-working.patch
Patch153: 0154-cern-Don-t-relabel-var-lib-kubelet-in-kubelet.servic.patch
Patch154: 0155-heat-agent-make-notify-heat-try-up-to-60-times.patch
Patch155: 0156-Revert-Populate-kube-controller-scheduler-proxy-IP.patch
Patch156: 0157-cern-Fix-cern-chart-cvmfs-csi-values-section-name.patch
Patch157: 0158-cern-drop-logtostderr-config-option.patch
Patch158: 0159-disable-prometheus-adapter-when-monitoring_enabled-i.patch
Patch159: 0160-cern-set-cern-magnum-helm-install-timeout-to-15m.patch
Patch160: 0161-Fix-cern-enable-autoscaler-label-not-working.patch
Patch161: 0162-cern-drop-dep-update-from-branch-helm-install.patch
Patch162: 0163-cern-Allow-OS-magnum-admin-to-get-patch-cluster-cert.patch



Source1:	%{service}.logrotate
Source2:	%{name}-api.service
Source3:	%{name}-conductor.service

BuildArch: noarch

BuildRequires: git
BuildRequires: python%{pyver}-devel
BuildRequires: python%{pyver}-pbr
BuildRequires: python%{pyver}-setuptools
BuildRequires: python%{pyver}-werkzeug
BuildRequires: systemd-units
BuildRequires: openstack-macros
# Required for config file generation
BuildRequires: python%{pyver}-pycadf
BuildRequires: python%{pyver}-osprofiler

Requires: %{name}-common = %{version}-%{release}
Requires: %{name}-conductor = %{version}-%{release}
Requires: %{name}-api = %{version}-%{release}

%description
%{common_desc}

%package -n python%{pyver}-%{service}
Summary: Magnum Python libraries
%{?python_provide:%python_provide python%{pyver}-%{service}}

Requires: python%{pyver}-pbr
Requires: python%{pyver}-babel
Requires: python%{pyver}-sqlalchemy
Requires: python%{pyver}-wsme
Requires: python%{pyver}-webob
Requires: python%{pyver}-alembic
Requires: python%{pyver}-docker >= 2.4.2
Requires: python%{pyver}-eventlet
Requires: python%{pyver}-iso8601
Requires: python%{pyver}-jsonpatch
Requires: python%{pyver}-keystonemiddleware >= 4.17.0
Requires: python%{pyver}-netaddr

Requires: python%{pyver}-oslo-concurrency >= 3.26.0
Requires: python%{pyver}-oslo-config >= 2:5.2.0
Requires: python%{pyver}-oslo-context >= 2.19.2
Requires: python%{pyver}-oslo-db >= 4.27.0
Requires: python%{pyver}-oslo-i18n >= 3.15.3
Requires: python%{pyver}-oslo-log >= 3.36.0
Requires: python%{pyver}-oslo-messaging >= 5.29.0
Requires: python%{pyver}-oslo-middleware >= 3.31.0
Requires: python%{pyver}-oslo-policy >= 1.30.0
Requires: python%{pyver}-oslo-serialization >= 2.18.0
Requires: python%{pyver}-oslo-service >= 1.24.0
Requires: python%{pyver}-oslo-utils >= 3.33.0
Requires: python%{pyver}-oslo-versionedobjects >= 1.31.2
Requires: python%{pyver}-oslo-reports >= 1.18.0
Requires: python%{pyver}-oslo-upgradecheck >= 0.1.1
Requires: python%{pyver}-osprofiler

Requires: python%{pyver}-pycadf
Requires: python%{pyver}-pecan

Requires: python%{pyver}-barbicanclient >= 4.5.2
Requires: python%{pyver}-glanceclient >= 1:2.8.0
Requires: python%{pyver}-heatclient >= 1.10.0
Requires: python%{pyver}-neutronclient >= 6.7.0
Requires: python%{pyver}-novaclient >= 9.1.0
Requires: python%{pyver}-kubernetes
Requires: python%{pyver}-keystoneclient >= 1:3.8.0
Requires: python%{pyver}-keystoneauth1 >= 3.4.0
Requires: python%{pyver}-octaviaclient >= 1.6.0
Requires: python%{pyver}-cinderclient >= 2.2.0

Requires: python%{pyver}-cliff >= 2.8.0
Requires: python%{pyver}-requests
Requires: python%{pyver}-six
Requires: python%{pyver}-stevedore >= 1.20.0
Requires: python%{pyver}-taskflow
Requires: python%{pyver}-cryptography
Requires: python%{pyver}-werkzeug
Requires: python%{pyver}-marathon

# Handle python2 exception
%if %{pyver} == 2
Requires: PyYAML
Requires: python-decorator
Requires: python-enum34
%else
Requires: python%{pyver}-PyYAML
Requires: python%{pyver}-decorator
%endif


%description -n python%{pyver}-%{service}
%{common_desc}

%package common
Summary: Magnum common

Requires: python%{pyver}-%{service} = %{version}-%{release}

Requires(pre): shadow-utils

%package -n python-%{service}-staging
Summary: Magnum Staging Drivers

Requires: python-%{service}

%description -n python-%{service}-staging
Magnum staging drivers package, used to deploy changes early in production
environments in a separate set of drivers.

%description common
Components common to all OpenStack Magnum services

%package conductor
Summary: The Magnum conductor

Requires: %{name}-common = %{version}-%{release}

%{?systemd_requires}

%description conductor
OpenStack Magnum Conductor

%package api
Summary: The Magnum API

Requires: %{name}-common = %{version}-%{release}

%{?systemd_requires}

%description api
OpenStack-native ReST API to the Magnum Engine

%if 0%{?with_doc}
%package -n %{name}-doc
Summary:    Documentation for OpenStack Magnum

Requires:    python%{pyver}-%{service} = %{version}-%{release}

BuildRequires:  python%{pyver}-sphinx
BuildRequires:  python%{pyver}-openstackdocstheme
BuildRequires:  python%{pyver}-stevedore
BuildRequires:  graphviz

%description -n %{name}-doc
%{common_desc}

This package contains documentation files for Magnum.
%endif

# tests
%package -n python%{pyver}-%{service}-tests
Summary:          Tests for OpenStack Magnum
%{?python_provide:%python_provide python%{pyver}-%{service}-tests}

Requires:        python%{pyver}-%{service} = %{version}-%{release}

BuildRequires:   python%{pyver}-fixtures
BuildRequires:   python%{pyver}-hacking
BuildRequires:   python%{pyver}-mock
BuildRequires:   python%{pyver}-oslotest
BuildRequires:   python%{pyver}-os-testr
BuildRequires:   python%{pyver}-subunit
BuildRequires:   python%{pyver}-stestr
BuildRequires:   python%{pyver}-testscenarios
BuildRequires:   python%{pyver}-testtools

# copy-paste from runtime Requires
BuildRequires: python%{pyver}-babel
BuildRequires: python%{pyver}-sqlalchemy
BuildRequires: python%{pyver}-wsme
BuildRequires: python%{pyver}-webob
BuildRequires: python%{pyver}-alembic
BuildRequires: python%{pyver}-docker >= 2.4.2
BuildRequires: python%{pyver}-eventlet
BuildRequires: python%{pyver}-iso8601
BuildRequires: python%{pyver}-jsonpatch
BuildRequires: python%{pyver}-keystonemiddleware
BuildRequires: python%{pyver}-netaddr

BuildRequires: python%{pyver}-oslo-concurrency
BuildRequires: python%{pyver}-oslo-config
BuildRequires: python%{pyver}-oslo-context
BuildRequires: python%{pyver}-oslo-db
BuildRequires: python%{pyver}-oslo-i18n
BuildRequires: python%{pyver}-oslo-log
BuildRequires: python%{pyver}-oslo-messaging
BuildRequires: python%{pyver}-oslo-middleware
BuildRequires: python%{pyver}-oslo-policy
BuildRequires: python%{pyver}-oslo-serialization
BuildRequires: python%{pyver}-oslo-service
BuildRequires: python%{pyver}-oslo-utils
BuildRequires: python%{pyver}-oslo-versionedobjects
BuildRequires: python%{pyver}-oslo-versionedobjects-tests
BuildRequires: python%{pyver}-oslo-reports
BuildRequires: python%{pyver}-oslo-upgradecheck

BuildRequires: python%{pyver}-pecan

BuildRequires: python%{pyver}-barbicanclient
BuildRequires: python%{pyver}-glanceclient
BuildRequires: python%{pyver}-heatclient
BuildRequires: python%{pyver}-neutronclient
BuildRequires: python%{pyver}-novaclient
BuildRequires: python%{pyver}-kubernetes
BuildRequires: python%{pyver}-keystoneclient
BuildRequires: python%{pyver}-octaviaclient
BuildRequires: python%{pyver}-cinderclient

BuildRequires: python%{pyver}-requests
BuildRequires: python%{pyver}-six
BuildRequires: python%{pyver}-stevedore
BuildRequires: python%{pyver}-taskflow
BuildRequires: python%{pyver}-cryptography
BuildRequires: python%{pyver}-marathon

# Handle python2 exception
%if %{pyver} == 2
BuildRequires: PyYAML
BuildRequires: python-decorator
BuildRequires: python-enum34
%else
BuildRequires: python%{pyver}-PyYAML
BuildRequires: python%{pyver}-decorator
%endif

%description -n python%{pyver}-%{service}-tests
%{common_desc}

%prep
%autosetup -n %{service}-%{upstream_version} -S git

# Let's handle dependencies ourselves
rm -rf {test-,}requirements{-bandit,}.txt tools/{pip,test}-requires

# Remove tests in contrib
find contrib -name tests -type d | xargs rm -rf

%build
%{pyver_build}

%install
%{pyver_install}

# docs generation requires everything to be installed first
%if 0%{?with_doc}
export PYTHONPATH=.
sphinx-build-%{pyver} -W -b html doc/source doc/build/html
# Fix hidden-file-or-dir warnings
rm -fr doc/build/html/.doctrees doc/build/html/.buildinfo
%endif

mkdir -p %{buildroot}%{_localstatedir}/log/%{service}/
mkdir -p %{buildroot}%{_localstatedir}/run/%{service}/
install -p -D -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}

# install systemd unit files
install -p -D -m 644 %{SOURCE2} %{buildroot}%{_unitdir}/%{name}-api.service
install -p -D -m 644 %{SOURCE3} %{buildroot}%{_unitdir}/%{name}-conductor.service

mkdir -p %{buildroot}%{_sharedstatedir}/%{service}/
mkdir -p %{buildroot}%{_sharedstatedir}/%{service}/certificates/
mkdir -p %{buildroot}%{_sysconfdir}/%{service}/

oslo-config-generator-%{pyver} --config-file etc/%{service}/magnum-config-generator.conf --output-file %{buildroot}%{_sysconfdir}/%{service}/magnum.conf
chmod 640 %{buildroot}%{_sysconfdir}/%{service}/magnum.conf
mv %{buildroot}%{_prefix}/etc/%{service}/api-paste.ini %{buildroot}%{_sysconfdir}/%{service}

# Remove duplicate config directory /usr/etc/magnum, we are keeping config files at /etc/magnum
rmdir %{buildroot}%{_prefix}/etc/%{service}



# prepare files to build a package with staging drivers.
# we do this by copying all drivers to corresponding _staging directories,
# including copying the common templates directory into the driver,
# and sed/replace so that staging drivers use the new common code already,
# even if the main python-magnum is not  yet installed.
#
# also change the os to be fedora-atomic-staging and ubuntu-staging, as this
# is the only way to currently distinguish drivers with the same coe.
#
# this assumes that a prod install will have python-magnum from the main repo,
# and python-magnum-staging installed from a different (qa) repo, so the changes
# (including the ones in common) get earlier to the staging drivers.
cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_v1 %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging
cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_v1 %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging
cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_v2 %{buildroot}%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging

cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/common %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging
cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/common %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging
cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/common %{buildroot}%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging

cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/heat %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging
cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/heat %{buildroot}%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging
cp -R %{buildroot}%{pyver_sitelib}/%{service}/drivers/heat %{buildroot}%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging

sed -i 's#../../common/#../common/#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/templates/kubemaster.yaml
sed -i 's#../../common/#../common/#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/templates/kubeminion.yaml
sed -i 's#fedora-atomic#fedora-atomic-staging#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/driver.py
sed -i 's#magnum.drivers.k8s_fedora_atomic_v1#magnum.drivers.k8s_fedora_atomic_staging#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/driver.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_atomic_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/driver.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_atomic_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/template_def.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_atomic_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/heat/k8s_template_def.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_atomic_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging/heat/k8s_fedora_template_def.py
find %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_atomic_staging -type f -exec sed -i 's#magnum.drivers.common#magnum.drivers.k8s_fedora_atomic_staging.common#' {} \;

sed -i 's#../../common/#../common/#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/templates/kubemaster.yaml
sed -i 's#../../common/#../common/#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/templates/kubeminion.yaml
sed -i 's#fedora-atomic#fedora-atomic-staging#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/driver.py
sed -i 's#magnum.drivers.k8s_fedora_atomic_v1#magnum.drivers.k8s_fedora_coreos_staging#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/driver.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_coreos_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/driver.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_coreos_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/template_def.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_coreos_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/heat/k8s_template_def.py
sed -i 's#magnum.drivers.heat#magnum.drivers.k8s_fedora_coreos_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging/heat/k8s_fedora_template_def.py
find %{buildroot}/%{pyver_sitelib}/%{service}/drivers/k8s_fedora_coreos_staging -type f -exec sed -i 's#magnum.drivers.common#magnum.drivers.k8s_fedora_coreos_staging.common#' {} \;

sed -i 's#../../common/#../common/#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging/templates/swarmmaster.yaml
sed -i 's#../../common/#../common/#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging/templates/swarmnode.yaml
sed -i 's#fedora-atomic#fedora-atomic-staging#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging/driver.py
sed -i 's#magnum.drivers.swarm_fedora_atomic_v2#magnum.drivers.swarm_fedora_atomic_staging#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging/driver.py
sed -i 's#magnum.drivers.heat#magnum.drivers.swarm_fedora_atomic_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging/driver.py
sed -i 's#magnum.drivers.heat#magnum.drivers.swarm_fedora_atomic_staging.heat#' %{buildroot}/%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging/template_def.py
find %{buildroot}/%{pyver_sitelib}/%{service}/drivers/swarm_fedora_atomic_staging -type f -exec sed -i 's#magnum.drivers.common#magnum.drivers.swarm_fedora_atomic_staging.common#' {} \;

%check

%files -n python%{pyver}-%{service}
%license LICENSE
%{pyver_sitelib}/%{service}
%{pyver_sitelib}/%{service}-*.egg-info
%exclude %{pyver_sitelib}/%{service}/tests
%exclude %{pyver_sitelib}/%{service}/drivers/*_staging


%files -n python-magnum-staging
%license LICENSE
%{pyver_sitelib}/%{service}/drivers/*_staging


%files common
%{_bindir}/magnum-db-manage
%{_bindir}/magnum-driver-manage
%{_bindir}/magnum-status
%license LICENSE
%dir %attr(0750,%{service},root) %{_localstatedir}/log/%{service}
%dir %attr(0755,%{service},root) %{_localstatedir}/run/%{service}
%dir %attr(0755,%{service},root) %{_sharedstatedir}/%{service}
%dir %attr(0755,%{service},root) %{_sharedstatedir}/%{service}/certificates
%dir %attr(0755,%{service},root) %{_sysconfdir}/%{service}
%config(noreplace) %{_sysconfdir}/logrotate.d/openstack-%{service}
%config(noreplace) %attr(-, root, %{service}) %{_sysconfdir}/%{service}/magnum.conf
%config(noreplace) %attr(-, root, %{service}) %{_sysconfdir}/%{service}/api-paste.ini
%pre common
# 1870:1870 for magnum - rhbz#845078
getent group %{service} >/dev/null || groupadd -r --gid 1870 %{service}
getent passwd %{service}  >/dev/null || \
useradd --uid 1870 -r -g %{service} -d %{_sharedstatedir}/%{service} -s /sbin/nologin \
-c "OpenStack Magnum Daemons" %{service}
exit 0


%files conductor
%doc README.rst
%license LICENSE
%{_bindir}/magnum-conductor
%{_unitdir}/%{name}-conductor.service

%post conductor
%systemd_post %{name}-conductor.service

%preun conductor
%systemd_preun %{name}-conductor.service

%postun conductor
%systemd_postun_with_restart %{name}-conductor.service


%files api
%doc README.rst
%license LICENSE
%{_bindir}/magnum-api
%{_unitdir}/%{name}-api.service


%if 0%{?with_doc}
%files -n %{name}-doc
%license LICENSE
%doc doc/build/html
%endif

%files -n python%{pyver}-%{service}-tests
%license LICENSE
%{pyver_sitelib}/%{service}/tests

%post api
%systemd_post %{name}-api.service

%preun api
%systemd_preun %{name}-api.service

%postun api
%systemd_postun_with_restart %{name}-api.service

%changelog
* Tue Jun 14 2022 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.27
- TBD

* Thu Jan 20 2022 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.26
- Ignore volume az in cinder config
- Install prometheus from cern-magnum chart
- Install cvmfs from cern-magnum chart

* Fri Dec 17 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.25
- Fix for calico IPIP with ipv6
- Setup dual stack / ipv6 for cluster 1.22+ only

* Wed Nov 24 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.24
- Add support for dual stack networking
- Add cert manager labels
- Add labels for snapshot controller
- Add NPD installation via helm chart
- Add nginx and traefik installation via helm chart
- Update coredns to 1.8.6
- Move metrics server to umbrella chart

* Tue Oct 09 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.23
- Fix for FCOS docker storage
- Update Calico CRD version to v1 (1.22 compatibility)
- Add label for CERN chart values
- Retrial for etcd discovery url
- Mount /etc/hosts on kubelet
- Explicitly set max log size for podman

* Tue Jun 22 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.22
- Explicit set cgroups to v1 (default v2 in fedora 34)
- Add ExtendedResourceToleration by default
- Replace manila provisioner with csi-manila
- Add missing cephfs storage classes (meyrin-cephfs-vault, ...)
- Move csi-cephfs installation to helm
- Add ignition_version label, set cgroups v1 explicitly
- Drop kubelet-https from kubelet-apiserver

* Mon Mar 29 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.21
- Fix issues with OIDC deployment
- Pass api address to kubelets
- Set dnsPolicy to ClusterFirstWithHostNet everywhere hostNetwork: true
- Poll kubernetes api with kubectl
- Make metrics server work without DNS

* Tue Mar 02 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.20
- Add support for OIDC
- Deploy separate certificates for kubelet and kube proxy
- Disable all insecure ports

* Thu Jan 07 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.19
- Set stable repo explicitly on helm init

* Tue Nov 17 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.18
- Rebuild with missing patches

* Fri Nov 13 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.17
- Add region name to occm config
- Increase nofiles on nodes
- Update stable helm chart location to charts.helm.sh

* Wed Oct 28 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.16
- Fix for EOS helm install
- Wait for umbrella chart helm install on cluster creation
- Fix use of CERN_CHART_ENABLED on write-heat-params
- Use nvidia_gpu_tag label on helm chart install
- Set coredns cache to 900 secs
- Configure scheduler policies for bin pack
- Introduce helm client tag label
- Allow configuration of helm client tag using label
- Update cluster roles for cluster autoscaler
- Update cluster autoscaler config for v1.19 version
- Set load balancer and network id in cloud config

* Wed Jul 15 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.15
- Add support for cinder csi
- Configure CERN base via umbrella chart (cern-magnum)
- Set containerRuntime in fluentd config

* Thu May 28 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.14
- Allow overriding cluster template and cluster labels
- Set replicas to 1 for CSI provisioners
- Update nginx ingress to v1.36.3
- Move fluentd, landb-sync, prometheus-cern to umbrella chart

* Thu Apr 30 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.13
- Add label for nvidia gpus
- Add configuration of cern helm meta chart
- Deploy traefik from heat agent
- Make fluentd work with selinux
- Enable server group affinity
- Enable cluster master load balancer

* Tue Apr 14 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.12
- OS-11210 Fix mounting nfs volumes

* Mon Apr 06 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.11
- OS-10908 Upgrade calico to v3.13.1 and coredns to 1.6.6

* Mon Mar 16 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.10
- OS-5454 Add containerd support

* Mon Mar 16 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.9
- OS-10946 Address high number of logs

* Thu Mar 12 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.8
- OS-10928 add missing capability in calico clusterrole

* Mon Feb 24 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.7
- OS-10869 Add selector to fluentd

* Tue Feb 18 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.6
- Add CMS specific config to cvmfs csi setup

* Tue Feb 11 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 9.2.0-1.5
- Update kubernetes APIs to apps/v1 for 1.18 compatibiility

* Tue Feb 04 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.4
- Fix traefik systemd deployment unit OS-10753
  Fix cern_enabled check OS-10770
  Disable auto-updates OS-10776

* Mon Feb 03 2020 Spyridon Trigazis <spyridon.trigazis@cern.ch> 9.2.0-1.3
- Fix cluster resize and update OS-10757

* Mon Feb 03 2020 Spyridon Trigazi <spyridon.trigazis@cern.ch> 9.2.0-1.2
- Fix traefik systemd deployment unit OS-10753

* Mon Feb 03 2020 Diogo Guerra <diogo.filipe.tomas.guerra@cern.ch> 9.2.0-1.1
- CERN Train release from 9.2.0

* Mon Jan 06 2020 RDO <dev@lists.rdoproject.org> 9.2.0-1
- Update to 9.2.0

* Wed Nov 06 2019 RDO <dev@lists.rdoproject.org> 9.1.0-1
- Update to 9.1.0

* Wed Oct 16 2019 RDO <dev@lists.rdoproject.org> 9.0.0-1
- Update to 9.0.0

* Fri Oct 11 2019 RDO <dev@lists.rdoproject.org> 9.0.0-0.2.0rc1
- Update to 9.0.0.0rc2

* Mon Sep 30 2019 RDO <dev@lists.rdoproject.org> 9.0.0-0.1.0rc1
- Update to 9.0.0.0rc1
